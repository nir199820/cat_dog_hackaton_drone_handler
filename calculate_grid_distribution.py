from generations.generateHeatmap import generate_heat_map
from graphql_handler.graphqlHandler import GraphQLRequests


def calculate_grid_distribution(url):
    graph_query_handler = GraphQLRequests(url)
    grid_distribution = generate_heat_map()
    open_events = graph_query_handler.import_events(True)
    closed_events = graph_query_handler.import_events(False)
    all_events = open_events + closed_events
    for event in all_events:
        grid_distribution[event.grid_cell.x][event.grid_cell.y] = {"cat": 0.0, "dog": 0.0, "parrot": 0.0, "rabbit": 0.0}
    return grid_distribution
