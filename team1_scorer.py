from team1_solution import send_drones


team1_score = 0
TOTAL_TESTS = 8

########################################################
###############     SCENARIO 1   #######################
########################################################
def create_empty_heat_map():
    """
    helper function the will generate an heat_map that has 0 probability for any animal in any rectangle.
    """
    return [[{"dog": 0.0, "cat": 0.0, "rabbit": 0.0, "parrot": 0.0} for j in range(100)] for i in range(100)]

def create_scenario1():
    heat_map = create_empty_heat_map()
    heat_map[3][3]["cat"] = 0.92
    heat_map[3][5]["cat"] = 0.9
    adopters_requests = {"adopter_1": ["rabbit", "cat"]}
    free_drones_locs = {"drone_1": (3,4)}
    busy_drones_dests = {}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, \
            take_picture_penalty, send_drone_penalty

optimal_solution_scenario_1 = {"drone_1": (3, 3)}
dss_solution_scenario_1 = send_drones(*create_scenario1())

if(optimal_solution_scenario_1 == dss_solution_scenario_1):
    team1_score += 1
    
########################################################
###############     SCENARIO 2   #######################
########################################################
def create_scenario2():
    heat_map = create_empty_heat_map()
    heat_map[76][36] = {"cat": 0.0, "dog": 0.33, "rabbit": 0.34, "parrot": 0.0}
    heat_map[72][40] = {"cat": 0.0, "parrot": 0.27, "rabbit": 0.36, "dog": 0.0}
    heat_map[68][36]["rabbit"] = 0.6
    adopters_requests = {"adopter_1": ["dog", "rabbit"], "adopter_2": ["rabbit", "parrot"]}
    free_drones_locs = {"drone_1": (72,36)}
    busy_drones_dests = {}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty


optimal_solution_scenario_2 = {"drone_1": (76, 36)}
dss_solution_scenario_2 = send_drones(*create_scenario2())

if(optimal_solution_scenario_2 == dss_solution_scenario_2):
    team1_score += 1
    
########################################################
###############     SCENARIO 3   #######################
########################################################
def create_scenario3():
    heat_map = create_empty_heat_map()
    heat_map[76][36] = {"cat": 0.0, "dog": 0.33, "rabbit": 0.34, "parrot": 0.0}
    heat_map[72][40] = {"cat": 0.0, "parrot": 0.27, "rabbit": 0.36, "dog": 0.0}
    heat_map[68][36]["rabbit"] = 0.6
    adopters_requests = {"adopter_1": ["dog", "rabbit"], "adopter_2": ["rabbit", "parrot"]}
    free_drones_locs = {"drone_1": (72,36), "drone_2": (70,38)}
    busy_drones_dests = {}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty


optimal_solution_scenario_3 = {"drone_1": (76,36), "drone_2": (72, 40)}
dss_solution_scenario_3 = send_drones(*create_scenario3())

if(optimal_solution_scenario_3 == dss_solution_scenario_3):
    team1_score += 1
    
########################################################
###############     SCENARIO 4   #######################
########################################################
def create_scenario4():
    heat_map = create_empty_heat_map()
    heat_map[10][40]["cat"] = 0.93
    heat_map[90][90]["rabbit"] = 0.93
    adopters_requests = {"adopter_1": ["cat", "rabbit"]}
    free_drones_locs = {"drone_1": (80,80)}
    busy_drones_dests = {}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty

optimal_solution_scenario_4 = {"drone_1": (90, 90)}
dss_solution_scenario_4 = send_drones(*create_scenario4())

if(optimal_solution_scenario_4 == dss_solution_scenario_4):
    team1_score += 1
    
########################################################
###############     SCENARIO 5   #######################
########################################################
def create_scenario5():
    heat_map = create_empty_heat_map()
    heat_map[13][7]["rabbit"] = 0.9
    heat_map[13][6]["parrot"] = 0.9
    heat_map[13][97]["parrot"] = 0.91
    adopters_requests = {"adopter_1": ["rabbit"], "adopter_2": ["parrot"]}
    free_drones_locs = {"drone_1": (13,2)}
    busy_drones_dests = {}
    take_picture_penalty = 100
    send_drone_penalty = 100
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty


optimal_solution_scenario_5 = {"drone_1": (13, 6)}
dss_solution_scenario_5 = send_drones(*create_scenario5())

if(optimal_solution_scenario_5 == dss_solution_scenario_5):
    team1_score += 1
    
########################################################
###############     SCENARIO 6   #######################
########################################################
def create_scenario6():
    heat_map = create_empty_heat_map()
    heat_map[15][5]["cat"] = 0.91
    heat_map[95][85]["cat"] = 0.9
    adopters_requests = {"adopter_1": ["cat"]}
    free_drones_locs = {"drone_2": (55,44)}
    busy_drones_dests = {"drone_1": (15,5)}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty


optimal_solution_scenario_6 = {"drone_2": (95, 85)}
dss_solution_scenario_6 = send_drones(*create_scenario6())

if(optimal_solution_scenario_6 == dss_solution_scenario_6):
    team1_score += 1
    
########################################################
###############     SCENARIO 7   #######################
########################################################
def create_scenario7():
    heat_map = create_empty_heat_map()
    heat_map[18][5]["dog"] = 0.95
    heat_map[96][83]["dog"] = 0.948
    adopters_requests = {"adopter_1": ["dog"]}
    free_drones_locs = {"drone_1": (57,44)}
    busy_drones_dests = {"drone_2": (17, 4)}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty

# for the above scenario, the optimal decision is to send drone1 to (89,89). although (11,11) has little higher probability for 
# matching adopter1 request, drone2 already is flying to a very closed rectangle to it ((10,10)), so we'll don't
# waste drone1 for 1, but instead will send it to the next likely rectangle to make a match - i.e. (89,89). later on, when
# drone_2 will arive to (10,10) we will send it to (11, 11) right after.
optimal_solution_scenario_7 = {"drone_1": (96, 83)}
dss_solution_scenario_7 = send_drones(*create_scenario7())

if(optimal_solution_scenario_7 == dss_solution_scenario_7):
    team1_score += 1
    
########################################################
###############     SCENARIO 8   #######################
########################################################
def create_scenario8():
    heat_map = create_empty_heat_map()
    heat_map[45][36]["cat"] = 0.8
    heat_map[69][36]["cat"] = 0.7
    adopters_requests = {"adopter_1": ["cat"], "adopter_2": ["cat"]}
    free_drones_locs = {"drone_1": (57,48), "drone_2": (57,24)}
    busy_drones_dests = {}
    take_picture_penalty = 1
    send_drone_penalty = 1
    return heat_map, adopters_requests, free_drones_locs, busy_drones_dests, take_picture_penalty, send_drone_penalty

# here the optimal solution should send each drone to another rectangle (it's not matter which to where). in this test
# we validate that the dss algorithm doesn't send both drones to the same rectangle:
optimal_solution_scenario_8 = {"drone_1": (45, 36), "drone_2": (69, 36)}
dss_solution_scenario_8 = send_drones(*create_scenario8())

if(sorted(optimal_solution_scenario_8.values()) == sorted(dss_solution_scenario_8.values())):
    team1_score += 1


def get_team1_score():
    return team1_score/TOTAL_TESTS * 100


