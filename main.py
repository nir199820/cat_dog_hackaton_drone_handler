from requests_handler import execute_drones
from Data.app_properties import thread_flags, port
from flask import Flask, request
from Data.app_properties import team_score, JAVA_server_url
import threading

drones_executor = threading.Thread(target=execute_drones, daemon=True)

threads = dict(drones_executor=drones_executor)

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def hello():
    if request.method == 'GET':
        return "Hello _GET"
    elif request.method == 'POST':
        return "Hello _POST"
    else:
        return None


@app.route('/get_status', methods=['POST', 'GET'])
def get_status():
    if thread_flags['drones_executor_flag']:
        return str(team_score)
    else:
        return str(200 + team_score)


@app.route('/send_drones', methods=['POST', 'GET'])
def send_quadcopters():
#   if request.method == 'POST':
    thread = threads['drones_executor']
    if thread.is_alive():
        thread_flags['drones_executor_flag'] = False
        print("finished Drones...")
        print("joining thread")
        thread.join()
        return "Drones are sent!"
    else:
        thread = threading.Thread(target=execute_drones, daemon=True)
        threads['drones_executor'] = thread
        thread_flags['drones_executor_flag'] = True
        print("Sending Drones...")
        thread.start()
#       return "In order to deploy the drones send a POST request to this endpoint"
        return "Stopped Drones!"


if __name__ == '__main__':
    send_quadcopters()
    print("starting app")
    app.run('0.0.0.0', 5001)
    send_quadcopters()

