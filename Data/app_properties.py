from calculate_grid_distribution import calculate_grid_distribution
from team1_scorer import get_team1_score
from requests import post, get

JAVA_server_port = '9001'
JAVA_server_ip = 'http://catndog.northeurope.cloudapp.azure.com'
JAVA_server_url = JAVA_server_ip+':'+JAVA_server_port+'/graphql'
JAVA_server_score_url = JAVA_server_ip+':'+JAVA_server_port+'/scores/challenge1'
grid_distribution = calculate_grid_distribution(JAVA_server_url)
team_score = int(get_team1_score())
print(get(JAVA_server_score_url, params={'grade': team_score}))
thread_flags = dict(drones_executor_flag=False)
port = 5001

