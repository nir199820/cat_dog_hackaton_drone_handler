from graphql_handler.graphqlHandler import GraphQLRequests, GraphQlMutation
from team1_solution import send_drones as send_drones
from generations.conversions import from_entity_parser
from Data.app_properties import JAVA_server_url, grid_distribution, thread_flags
import time


def execute_drones():
    while thread_flags['drones_executor_flag']:
        if not thread_flags['drones_executor_flag']:
            break
        graph_query_handler = GraphQLRequests(JAVA_server_url)
        graph_mutation_handler = GraphQlMutation(JAVA_server_url)
        adopters = graph_query_handler.import_adopters()
        adopters_as_dictionary = from_entity_parser.adopters_dictionary(adopters)
        drones = graph_query_handler.import_quads()
        free_drones, busy_drones = from_entity_parser.free_drones(drones), from_entity_parser.busy_drones(drones)
        drones_to_send = send_drones(grid_distribution, adopters_as_dictionary, free_drones, busy_drones, take_picture_penalty=1, send_drone_penalty=1)
        for drone in drones_to_send.items():
            if not thread_flags['drones_executor_flag']:
                break
            x, y = drone[1]
            id = drone[0]
            graph_mutation_handler.create_event(int(id), x, y)
            grid_distribution[x][y] = {"cat": 0.0, "dog": 0.0, "parrot": 0.0, "rabbit": 0.0}
        print('sending drone')
        time.sleep(1)
